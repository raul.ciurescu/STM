#include "stm32f4xx_hal.h"

extern UART_HandleTypeDef huart3;

int _write(int file , char *data , int len)
{
	HAL_UART_Transmit(&huart3 , (uint8_t *) data , len , HAL_MAX_DELAY);

	return len;
}

int _read(int file, char *data, int len) {
    uint16_t index = 0;
    char ch;


    while (index < len - 1) {
        HAL_UART_Receive(&huart3, (uint8_t*)&ch, 1, HAL_MAX_DELAY);

        if (ch == '\r') {
            continue;
        }
        if ((ch == '\n') || index >= len ) {
            break;
        }

        data[index++] = ch;
    }

    data[index] = '\0';
    return index;
}

void UART_FlushBuffer(UART_HandleTypeDef *huart)
{
	uint8_t dummy;
	while (HAL_UART_Receive(huart , &dummy , 1 , 100) == HAL_OK)
	{

	}
}

void UART_ReadUntilChar(char *buffer, uint16_t bufferSize, char stopChar) {
    uint16_t index = 0;
    char ch;

    while (index < bufferSize - 1) {
        HAL_UART_Receive(&huart3, (uint8_t*)&ch, 1, HAL_MAX_DELAY);

        if (ch == '\r') {
            continue;
        }
        if (ch == '\n' || ch == stopChar) {
            break;
        }

        buffer[index++] = ch;
    }

    buffer[index] = '\0';
}

void CEVA(void)
{

	char ch;

	HAL_UART_Receive(&huart3 , (uint8_t *) &ch, 1 , HAL_MAX_DELAY);

	HAL_UART_Transmit(&huart3 , (uint8_t *) &ch , 1 , HAL_MAX_DELAY);

}
